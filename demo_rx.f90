program demo_rx
use RS232
implicit none

    integer::cport, speed
    character(3)::mode
    character(1024)::buf
    integer::n
    
    speed = 9600
    mode = '8N1'
    cport = 0
    
    if(.NOT. OpenComport(cport, speed, mode)) then
        Print *, "Could not open comport ", cport
        STOP
    end if
    
    Print *, "Starting polling..."
    
    do while(.TRUE.)
        
        n = PollComport(cport, buf, 1024)
        if(n .GT. 0) then
            Write(*, '(1X, I4, 2X, A15, 1X, A)') &
              n, "bytes received:", buf(1:n)
        end if
    
        call sleep(1)
    
    end do
    
end program demo_rx
