program demo_tx
use RS232
implicit none

    integer::cport, speed
    character(3)::mode
    
    speed = 9600
    mode = '8N1'
    cport = 0
    
    if(.NOT. OpenComport(cport, speed, mode)) then
        Print *, "Could not open comport ", cport
        STOP
    end if
    
    Print *, "Starting send..."
    
    do while(.TRUE.) 
        call cputs(cport, "Serial Transfer from Fortran!!!"//NEW_LINE('c'))
        call SLEEP(1)
    end do

end program demo_tx