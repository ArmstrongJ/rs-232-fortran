!    RS-232 Fortran Wrapper
!    Copyright 2017 Approximatrix, LLC <support@approximatrix.com>
!
!    The RS-232 Library is free software: you can redistribute it and/or 
!    modify it under the terms of the GNU General Public License as
!    published by the Free Software Foundation, either version 3 of the
!    License, or (at your option) any later version.
!
!    The RS-232 Library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    General Public License for more details.
!
!    You should have received a copy of the GNU General Public 
!    License along with the RS-232 Library.  If not, see 
!    <http://www.gnu.org/licenses/>.

module RS232
use ISO_C_BINDING
implicit none

   interface 
        function IsDCDEnabled_c(comport_number) bind(c, name='RS232_IsDCDEnabled')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        integer(kind=c_int)::IsDCDEnabled_c
        end function IsDCDEnabled_c
    end interface
    
    interface 
        function IsCTSEnabled_c(comport_number) bind(c, name='RS232_IsCTSEnabled')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        integer(kind=c_int)::IsCTSEnabled_c
        end function IsCTSEnabled_c
    end interface
    
    interface 
        function IsDSREnabled_c(comport_number) bind(c, name='RS232_IsDSREnabled')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        integer(kind=c_int)::IsDSREnabled_c
        end function IsDSREnabled_c
    end interface
    
    interface
        subroutine enableDTR(comport_number) bind(c, name='RS232_enableDTR')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine enableDTR
    end interface

    interface
        subroutine disableDTR(comport_number) bind(c, name='RS232_disableDTR')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine disableDTR
    end interface
    
    interface
        subroutine enableRTS(comport_number) bind(c, name='RS232_enableRTS')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine enableRTS
    end interface
    
    interface
        subroutine disableRTS(comport_number) bind(c, name='RS232_disableRTS')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine disableRTS
    end interface
    
    interface
        subroutine flushRX(comport_number) bind(c, name='RS232_flushRX')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine flushRX
    end interface
    
    interface
        subroutine flushTX(comport_number) bind(c, name='RS232_flushTX')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine flushTX
    end interface
    
    interface
        subroutine flushRXTX(comport_number) bind(c, name='RS232_flushRXTX')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine flushRXTX
    end interface
    
    interface
        function GetPortnr_c(devname) bind(c, name='RS232_GetPortnr')
        use ISO_C_BINDING, only: c_int, c_char
        character(kind=c_char), dimension(*)::devname
        integer(kind=c_int)::GetPortnr_c
        end function GetPortnr_c
    end interface
    
    interface
        function OpenComport_c(comport_number, baudrate, mode) bind(c, name='RS232_OpenComport')
        use ISO_C_BINDING, only: c_int, c_char
        integer(kind=c_int), value::comport_number, baudrate
        character(kind=c_char), dimension(*)::mode
        integer(kind=c_int)::OpenComport_c
        end function OpenComport_c
    end interface

    interface
        function PollComport_c(comport_number, buffer, bufsize) bind(c, name='RS232_PollComport')
        use ISO_C_BINDING, only: c_int, c_char
        integer(kind=c_int), value::comport_number, bufsize
        character(kind=c_char), dimension(*)::buffer
        integer(kind=c_int)::PollComport_c
        end function PollComport_c
    end interface

    interface
        function SendByte_c(comport_number, byte) bind(c, name='RS232_SendByte')
        use ISO_C_BINDING, only: c_int, c_char
        integer(kind=c_int), value::comport_number
        character(kind=c_char), value::byte
        integer(kind=c_int)::SendByte_c
        end function SendByte_c
    end interface

    interface SendByte
        module procedure SendByte_character, SendByte_integer
    end interface SendByte

    interface 
        function SendBuf_c(comport_number, buffer, bufsize) bind(c, name='RS232_SendBuf')
        use ISO_C_BINDING, only: c_int, c_char
        integer(kind=c_int), value::comport_number, bufsize
        character(kind=c_char), dimension(*)::buffer
        integer(kind=c_int)::SendBuf_c
        end function SendBuf_c
    end interface
    
    interface
        subroutine CloseComport(comport_number) bind(c, name='RS232_CloseComport')
        use ISO_C_BINDING, only: c_int
        integer(kind=c_int), value::comport_number
        end subroutine CloseComport
    end interface
    
    interface
        subroutine cputs_c(comport_number, buffer) bind(c, name='RS232_cputs')
        use ISO_C_BINDING, only: c_int, c_char
        integer(kind=c_int), value::comport_number
        character(kind=c_char), dimension(*)::buffer
        end subroutine cputs_c
    end interface

contains

    function GetPortnr(devname)
    use ISO_C_BINDING
    implicit none
    
    character(*), intent(in)::devname
    integer::GetPortnr
    
        GetPortnr = GetPortnr_c(trim(devname)//C_NULL_CHAR)

    end function GetPortnr
    
    function OpenComport(comport_number, baudrate, mode)
    use ISO_C_BINDING
    implicit none
    
    integer, intent(in)::comport_number, baudrate
    character(*), intent(in)::mode
    logical::OpenComport
    integer::res
    
        res = OpenComport_c(comport_number, baudrate, trim(mode)//C_NULL_CHAR)
        OpenComport = (res .NE. 1)

    end function OpenComport
    
    function PollComport(comport_number, buffer, bufsize)
    use ISO_C_BINDING
    implicit none
    
    integer, intent(in)::comport_number, bufsize
    character(*), intent(inout)::buffer
    integer::PollComport
    
    integer::ret, i
    character(kind=c_char), dimension(:), allocatable::buffer_c
    
        allocate(buffer_c(bufsize))
    
        ret = PollComport_c(comport_number, buffer_c, bufsize)
        do i = 1, ret
            buffer(i:i) = buffer_c(i)
        end do
        
        deallocate(buffer_c)
        
        PollComport = ret
        
    end function PollComport
    
    function SendByte_character(comport_number, byte)
    use ISO_C_BINDING
    implicit none
    
    integer, intent(in)::comport_number
    character, intent(in)::byte
    logical::SendByte_character

        SendByte_character = (SendByte_c(comport_number, byte) .NE. 1)
        
    end function SendByte_character

    function SendByte_integer(comport_number, byte)
    use ISO_C_BINDING, only: C_CHAR
    implicit none
    
    integer, intent(in)::comport_number, byte
    logical::SendByte_integer

        SendByte_integer = (SendByte_c(comport_number, char(byte, kind=c_char)) .NE. 1)
        
    end function SendByte_integer
    
    function SendBuf(comport_number, buffer, bufsize)
    implicit none
    
    integer, intent(in)::comport_number, bufsize
    character(*), intent(in)::buffer
    integer::SendBuf
    
        SendBuf = SendBuf_c(comport_number, buffer, bufsize)
        
    end function SendBuf
    
    subroutine cputs(comport_number, buffer)
    use ISO_C_BINDING
    implicit none
    
    integer, intent(in)::comport_number
    character(*), intent(in)::buffer
    
        call cputs_c(comport_number, trim(buffer)//C_NULL_CHAR)
    
    end subroutine cputs
    
    function IsDSREnabled(comport_number)
    implicit none
    
    integer, intent(in)::comport_number
    logical::IsDSREnabled
    
        IsDSREnabled = (IsDSREnabled_c(comport_number) .EQ. 1)
        
    end function IsDSREnabled
    
    function IsCTSEnabled(comport_number)
    implicit none
    
    integer, intent(in)::comport_number
    logical::IsCTSEnabled
    
        IsCTSEnabled = (IsCTSEnabled_c(comport_number) .EQ. 1)
        
    end function IsCTSEnabled
    
    function IsDCDEnabled(comport_number)
    implicit none
    
    integer, intent(in)::comport_number
    logical::IsDCDEnabled
    
        IsDCDEnabled = (IsDCDEnabled_c(comport_number) .EQ. 1)
        
    end function IsDCDEnabled

end module RS232
